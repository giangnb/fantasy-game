﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSlot : MonoBehaviour
{
    [Header("Weapon")]
    public GameObject weaponLeft;
    public GameObject weaponRight;
    [Header("Brace")]
    public GameObject braceLeft;
    public GameObject braceRight;
    [Header("Shoulder")]
    public GameObject shoulderLeft;
    public GameObject shoulderRight;
    [Header("Helmet")]
    public GameObject helmet;
    [Header("Body")]
    public GameObject body;
}
